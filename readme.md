# redditautoacc

This is a tool for automatically creating accounts on reddit.

## Setup

### Requirements
* node.js (tested with node16)
* pnpm (can be install with `npm i -g pnpm`)
* firefox or chrome/chromium (if firefox, you also need [geckodriver](https://github.com/mozilla/geckodriver/releases))

### Steps
1. Clone the repo with `git clone https://gitlab.com/swz-git/redditautoacc`
2. Install dependencies with `pnpm i`
3. Bundle the typescript files to a single js file with `pnpm build`
4. Done! you can now use `pnpm start` to run the script (see [running](#running) for more info)

## Running

Custom settings are set via environment variables, heres a list of all of them:

* `CUSTOMEMAIL=bob@example.com` sets the email of every user to the email specified (you can only use one email per account, so you can't leave it running with this flag set).
* `AUTOVERIFYEMAIL=1` enables auto email verifying, it generates a temporary email address and uses that to sign up for every account.
* `BROWSER=firefox` can be either `chrome` or `firefox`. Default is chrome
* `ACCOUNTS_JSON=../placer-py/config/accounts.json` writes the account to a json file (for use with placer-py). Default is an empty string.

Start scripts are setup already at scripts/start.bat or scripts/start.sh