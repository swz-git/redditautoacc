import {
  Builder,
  By,
  Capabilities,
  Key,
  until,
  WebDriver,
} from "selenium-webdriver";
import inquirer from "inquirer";
import Mailjs from "@cemalgnlts/mailjs";
//@ts-ignore - esbuild lets us import files
import names from "./names.txt";
import { readFileSync, writeFileSync } from "fs";
import { Options } from "selenium-webdriver/chrome";
const namelist = names.split(/\r?\n/);

function wait(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function getUsername() {
  return (
    namelist[Math.round(Math.random() * namelist.length)] +
    Math.random().toString(36).substring(7)
  ).toLowerCase();
}
function getPassword() {
  return (
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7)
  );
}
async function getEmail(mailjs) {
  if (process.env.CUSTOMEMAIL) {
    return await inquirer
      .prompt([
        {
          name: "email",
          type: "input",
          message: "You need to provide an email address for this account",
        },
      ])
      .then((answers) => answers.email);
  } else if (process.env.AUTOVERIFYEMAIL) {
    // console.log(mailjs);
    console.log("Creating new email...");
    let registerresult = await mailjs.createOneAccount();
    const address = registerresult.data.username;
    const password = registerresult.data.password;
    if (!registerresult.status) {
      throw "Error creating email";
    }
    console.log("Created email!", address);
    console.log("Logging in to email...");
    let loginresult = await mailjs.login(address, password);
    if (!loginresult.status) {
      throw "Error logging into email";
    }
    console.log("Logged into email!");
    return address;
  }
  return (
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    "@example.com"
  );
}

async function main() {
  try {
    readFileSync(process.env.ACCOUNTS_JSON, "utf-8");
  } catch (e) {
    console.error("Couldn't write to accounts.json file");
    throw e;
  }
  const username = getUsername();
  const password = getPassword();
  const mailjs = new Mailjs();
  const email = await getEmail(mailjs);

  let driver: any = await new Builder()
    .setChromeOptions(new Options().addArguments("--disable-notifications"))
    .forBrowser(process.env.BROWSER || "chrome")
    .build();

  await driver.get("https://old.reddit.com/register");
  console.log(
    `Creating user with username: ${username} and password: ${password}`
  );

  await driver.findElement(By.id("user_reg")).sendKeys(username);
  await driver.findElement(By.id("passwd_reg")).sendKeys(password);
  await driver.findElement(By.id("passwd2_reg")).sendKeys(password);
  await driver.findElement(By.id("email_reg")).sendKeys(email);

  await inquirer.prompt([
    {
      name: "captcha",
      type: "confirm",
      message: "Press enter when you've completed the captcha",
    },
  ]);

  try {
    await driver
      .findElement(By.css(".c-btn.c-btn-primary.c-pull-right"))
      .click();
  } catch (e) {
    console.log("Couldn't press register, assuming user pressed register");
  }

  console.log("Account created!");
  if (process.env.AUTOVERIFYEMAIL) {
    let messages;
    while (1) {
      console.log("Waiting for verification email...");
      await wait(1000);
      messages = await mailjs.getMessages();
      if (messages.data.length > 0) {
        console.log("Email found! Verifying");
        break;
      }
    }

    let verificationEmail = await mailjs.getMessage(messages.data[0].id);
    let veriEmailhHtml = verificationEmail.data.html.join("");
    let veriLink = veriEmailhHtml.match(
      /[^"]+reddit\.com\/verification[^"]+/g
    )[0];

    await driver.get(veriLink);
    console.log("Verified email!");
    await wait(2000);
    console.log("Upvoting first post to avoid bot detection");
    await driver.findElement(By.css(".icon-upvote")).click();
  }

  await wait(1000);
  await driver.quit();

  if (process.env.ACCOUNTS_JSON) {
    console.log("Writing to accounts.json");
    let accountList = JSON.parse(
      readFileSync(process.env.ACCOUNTS_JSON, "utf-8")
    );
    accountList.push({ user: username, pass: password });
    writeFileSync(
      process.env.ACCOUNTS_JSON,
      JSON.stringify(accountList, null, 2)
    );
    console.log("Done!");
  }
}

// Loop until user stops the script
async function loop() {
  await main();
  let now = new Date();
  let in10m = new Date();
  in10m.setMinutes(now.getMinutes() + 10);
  console.log(
    "Waiting for 10min before creating another account",
    `(time is ${now.getHours()}:${now.getMinutes()}, running again at ${in10m.getHours()}:${in10m.getMinutes()})`
  );
  await wait(1000 * 60 * 10);
  loop();
}

loop();
